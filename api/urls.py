
from django.urls import path

from api.views.item import ItemList, ItemDetail
from api.views.asset_bundle import AssetBundleList, AssetBundleDetail
from api.views.upload import UploadImage
from api.views.like import LikeItem
from api.views.comment import CommentItem
# from api.auth import auth_urls

app_name = 'item'

urlpatterns = [
    path('items/', ItemList.as_view(), name='item-list'),
    path('items/<int:pk>/', ItemDetail.as_view(), name='item-detail'),
    path('assets/', AssetBundleList.as_view(), name='assets-list'),
    path('assets/<int:pk>/', AssetBundleDetail.as_view(), name='item-detail'),
    path('media/image/', UploadImage.as_view(), name='upload'),
    path('like/', LikeItem.as_view(), name='like'),
    path('comment/', CommentItem.as_view(), name='like'),
]
