from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField, SerializerMethodField
from api.models import Profile



class ProfileSerializer(ModelSerializer):

    """ User Profile Serializers """

    class Meta:
        model = Profile

        fields = (
            'id',
            'bio',
            'website_url'

        )
        read_only_fields = ('id',)