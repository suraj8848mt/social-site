from django.contrib.auth.models import User
from rest_framework import serializers

from api.serializers.profile import ProfileSerializer


class UserSerializer(serializers.ModelSerializer):
    """
    User Detail Serializer
    """
    profile = ProfileSerializer(many=False)
    groups = serializers.SerializerMethodField('get_user_groups')
    def get_user_groups(self, user):
        results = []
        for group in user.groups.all():
            results.append(group.name)
        
        return results

    class Meta:
        model = User
        fields = (
            'id',
            User.USERNAME_FIELD,
            'first_name',
            'last_name',
            'email',
            'profile',
            'groups',
        )
        read_only_field = ('id', 'groups', 'profile')


class UserDetailSerializer(UserSerializer):
    """ User Detail Serializer"""

    class Meta:
        model = User 
        fields = (
            'id',
            User.USERNAME_FIELD,
            'first_name',
            'last_name',
            'email',
            'groups',
        )
        read_only_fields = fields



class UserListSerializer(UserSerializer):
    """
        User List Serializer
    """

    class Meta:
        model = User
        fields = (
            'id',
            User.USERNAME_FIELD,
            'first_name',
            'last_name',
            'eamil',
        )
        read_only_fields = fields
