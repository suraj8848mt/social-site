from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault
from api.models import Item
from api.serializers.user import UserSerializer
from api.serializers.asset_bundle import AssetBundleDetailSerializer


# class ItemSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     title = serializers.CharField(required=False, allow_blank=True, max_length=255)
#     subtitle = serializers.CharField(required=False, allow_blank=True, max_length=255)
#     owner_id = serializers.IntegerField(required=True)
#     created_at = serializers.DateTimeField(read_only=True)
#     updated_at = serializers.DateTimeField(read_only=True)

#     def create(self, validated_data):
#         return Item.objects.create(**validated_data)


class ItemSerializer(serializers.ModelSerializer):
    """
    Item List & Detail Serializers
    """

    # owner = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    owner = serializers.PrimaryKeyRelatedField(read_only=True, default=CurrentUserDefault())

    class Meta:
        model = Item
        fields = (
            'id',
            'asset_bundle',
            'owner',
            'created_at',
        )
        read_only_fields = ('id',)


class ItemDetailSerializers(serializers.ModelSerializer):

    """
    Item Detail Serializers
    """

    owner = UserSerializer(many=False, read_only=True)
    asset_bundle = AssetBundleDetailSerializer(many=False, read_only=True)

    class Meta:
        model = Item
        fields = (
            'id',
            'owner',
            'total_likes',
            'likes',
            'total_comment',
            'comments',
            'asset_bundle',
            'created_at',
            'updated_at',
        )
        read_only_fields = ('id', 'owner_id')
