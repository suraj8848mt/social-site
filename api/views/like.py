from django.conf import settings
from rest_framework import status, mixins
from django.db import IntegrityError
from rest_framework.response import Response
from api import settings as api_settings
from rest_framework import generics
from api.models import Like, Item
from api.serializers.item import ItemDetailSerializers


import json



class LikeItem(generics.CreateAPIView):
    def post(self, request):
        data = json.loads(request.body.decode('utf-8'))

        if not 'item_id' in data:
            return Response({'error': 'No item id in request'}, status=status.HTTP_400_BAD_REQUEST)
       
        item = Item.objects.get(pk=data['item_id'])
        try:
            like = Like()
            like.item = item
            like.owner = request.user
            like.save()
        except IntegrityError as e:
            return Response({'error':'You already Liked this....'}, status=status.HTTP_400_BAD_REQUEST)
      

        serializer = ItemDetailSerializers(item)
        return Response(serializer.data, status=status.HTTP_200_OK)
