from django.conf import settings
from rest_framework import status, mixins
from rest_framework.response import Response
from api import settings as api_settings
from rest_framework import generics
from api.models import Like, Item, Comment
from api.serializers.item import ItemDetailSerializers

import json



class CommentItem(generics.CreateAPIView):
    def post(self, request):
        data = json.loads(request.body.decode('utf-8'))

        if not 'item_id' in data:
            return Response({'error': 'No item id in request'}, status=status.HTTP_400_BAD_REQUEST)

        if not 'body' in data:
            return Response({'error': 'No Body in request'}, status=status.HTTP_400_BAD_REQUEST)


        item = Item.objects.get(pk=data['item_id'])
        
        comment = Comment()
        comment.item = item
        comment.body = data['body']
        comment.owner = request.user
        comment.save()

        serializer = ItemDetailSerializers(item)
        return Response(serializer.data, status=status.HTTP_200_OK)
