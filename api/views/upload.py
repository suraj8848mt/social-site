
from django.conf import settings
from rest_framework import status, mixins
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView


from api.models import Asset, AssetBundle, Item
from api.serializers.item import ItemDetailSerializers

from celery import shared_task, Task
from celery.task import task
import cloudinary
import cloudinary.uploader
import cloudinary.api
import cloudinary

from PIL import Image
from io import BytesIO
import json, base64, string, random, os
import boto3, PIL
import os

 

class UploadImage(CreateAPIView):
    """ Image Upload View """
    def post(self, request):

        
        data = json.loads(request.body.decode('utf-8'))

        if not 'image' in data:
            return Response({'error': 'no image in request.'}, status=status.HTTP_400_BAD_REQUEST)
        if not 'mime' in data:
            return Response({'error': 'no mime in request.'}, status=status.HTTP_400_BAD_REQUEST)

        mime = data['mime']
        if not mime in ['image/jpeg', 'image/png', 'image/gif']:
            return Response({'error': 'mime not accepted. Must be either image/jpeg, image/png, or image/gif'}, status=status.HTTP_400_BAD_REQUEST)

        salt = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(16))
        image_string = data['image']
        image_string = image_string.replace('data:%s;base64,' % mime, '')

    
        img = Image.open(BytesIO(base64.b64decode(image_string)))
        filename = '%s.jpg' % salt
        temp_location = './temp/%s' % filename
        
        img.save(temp_location)


        upload_data = cloudinary.uploader.upload(temp_location)


       
        # upd = uploading.delay(temp_location )

        asset_bundle = AssetBundle()
        asset_bundle.salt = salt
        asset_bundle.kind = 'image'
        asset_bundle.base_url = upload_data.get('secure_url')
        asset_bundle.owner = request.user
        asset_bundle.save()

        asset = Asset()
        asset.asset_bundle = asset_bundle
        asset.kind = 'original'
        asset.width = img.width
        asset.height = img.height
        asset.extension = 'jpg'
        asset.loading = True
        asset.save()

            

        item = Item()
        item.asset_bundle = asset_bundle
        item.owner = request.user
        item.save()
        

        os.remove(temp_location)

        serializer = ItemDetailSerializers(item)
       
        return Response(serializer.data, status=status.HTTP_200_OK)




    

