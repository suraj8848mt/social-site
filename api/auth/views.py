from django.contrib.auth import get_user_model
from django.conf import settings as dj_settings
from rest_framework import status
from rest_framework.generics import RetrieveUpdateAPIView
from api import generics
from rest_framework.response import Response
from api.auth.utils import AuthTools
from api import settings as api_settings
from api.auth import serializers
from api.serializers.user import UserSerializer
from api.serializers.profile import ProfileSerializer
from api.auth.serializers import LoginSerializer, UserRegisterSerializer, LoginCompleteSerializer, LogoutSerializer
from api.models import Profile
import re


User = get_user_model()



class UserView(generics.RetrieveUpdateAPIView):
    """ User View """

    model = User
    serializer_class = UserSerializer
    permission_classes = api_settings.CONSUMER_PERMISSIONS

    def get_object(self, *args, **kwargs):
        return self.request.user


class ProfileView(RetrieveUpdateAPIView):
    """
        Profile View
    """

    model = User.profile
    serializer_class = ProfileSerializer
    permissions_classes = api_settings.CONSUMER_PERMISSIONS

    def get_object(self, *args, **kwargs):
        return self.request.user.profile


class LoginView(generics.GenericAPIView):
    """ Login View """

    permission_classes = api_settings.UNPROTECTED
    serializer_class = LoginSerializer

    def post(self, request):
        if 'email' in request.data and 'password' in request.data:
            email = request.data['email'].lower()
            password = request.data['password']

            user = AuthTools.authenticate_email(email, password)
            print(user)

            if user is not None and AuthTools.login(request, user):
                token = AuthTools.issue_user_token(user, 'login')
                print(token)
                serializer = serializers.LoginCompleteSerializer(token)
                return Response(serializer.data)
        
        message = {'message': 'Unable to login with the credential provided.'}
        return Response(message, status=status.HTTP_400_BAD_REQUEST)


class LogoutView(generics.GenericAPIView):
    """
        Logout Views
    """

    permission_classes = api_settings.CONSUMER_PERMISSIONS
    serializer_class = LogoutSerializer

    def post(self, request):
        if AuthTools.logout(request):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class RegisterView(generics.CreateAPIView):
    """
        Register View
    """

    serializer_class = serializers.UserRegisterSerializer
    permission_classes = api_settings.UNPROTECTED

    def perform_create(self, serializer):
        instance = serializer.save()
