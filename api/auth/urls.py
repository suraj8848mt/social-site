
from django.urls import path

from api.auth import views

app_name = 'auth'

urlpatterns = [
    path('me/', views.UserView.as_view(), name='user'),
    path('me/profile/', views.ProfileView.as_view(), name='profile'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('register/', views.RegisterView.as_view(), name='register'),
    
    
]
